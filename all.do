DEPS="log/log.o plasma.o"
redo-ifchange $DEPS
FLAGS=${DEBUG:--Ofast}
gcc ${FLAGS} -Ilog -o $3 $DEPS -lm
